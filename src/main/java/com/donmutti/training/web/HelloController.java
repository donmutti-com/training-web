package com.donmutti.training.web;

import com.donmutti.training.web.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private final HelloWorldService helloService;

    @Autowired
    public HelloController(HelloWorldService helloService) {
        this.helloService = helloService;
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String getHello() {
        return helloService.getHelloMessage();
    }
}
