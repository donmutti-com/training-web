package com.donmutti.training.web;

import com.donmutti.training.web.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingWebApplication implements CommandLineRunner {

    private final HelloWorldService helloWorldService;

    @Autowired
    public TrainingWebApplication(HelloWorldService helloWorldService) {
        this.helloWorldService = helloWorldService;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TrainingWebApplication.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println(this.helloWorldService.getHelloMessage());
    }
}
