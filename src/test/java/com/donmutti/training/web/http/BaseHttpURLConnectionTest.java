package com.donmutti.training.web.http;

import com.donmutti.training.web.BaseTest;
import org.junit.Ignore;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertNotNull;

/**
 * Base class for unit tests of {@link java.net.HttpURLConnection}/{@link javax.net.ssl.HttpsURLConnection}.
 */
@Ignore
abstract class BaseHttpURLConnectionTest extends BaseTest {

    /**
     * Opens HTTP connection
     * @param urlString URL
     * @return open HttpURLConnection
     * @throws IOException
     */
    HttpURLConnection createConnection(String urlString) throws IOException {
        LOGGER.info("Creating connection to {}...", urlString);
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        LOGGER.info("Connection created.");
        return conn;
    }

    /**
     * Opens HTTPS connection
     * @param urlString URL
     * @return open HttpsURLConnection
     * @throws IOException
     */
    HttpsURLConnection createSslConnection(String urlString) throws IOException {
        LOGGER.info("Creating SSL connection to {}...", urlString);
        URL url = new URL(urlString);
        HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        assertNotNull(conn);
        LOGGER.info("Connection created.");
        return conn;
    }

    void connect(HttpURLConnection conn) throws IOException {
        LOGGER.info("Connecting...");
        conn.connect();
        LOGGER.info("Connected.");
    }

    void disconnect(HttpURLConnection conn) throws IOException {
        LOGGER.info("Disconnecting...");
        conn.disconnect();
        LOGGER.info("Disconnected.");
    }

    int getResponse(HttpURLConnection conn) throws IOException {
        int responseCode = conn.getResponseCode();
        String responseMessage = conn.getResponseMessage();
        LOGGER.info("{} {}", responseCode, responseMessage);
        return responseCode;
    }
}
