package com.donmutti.training.web.unirest;

import com.donmutti.training.web.BaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Testing HTTP operations with <a href="http://unirest.io/">Unirest</a>.
 */
public class UnirestTest extends BaseTest {

    @Test
    public void testGet() throws Exception {
        HttpResponse<JsonNode> response = Unirest.get(URL_HTTPBIN_GET)
                                                 .queryString("q", "test")
                                                 .asJson();
        assertNotNull(response);
        assertEquals(200, response.getStatus());
        assertNotNull(response.getBody());
        ObjectMapper mapper = new ObjectMapper();
        com.fasterxml.jackson.databind.JsonNode json = mapper.readTree(response.getRawBody());
        assertEquals("test", json.at("/args/q").asText());
    }
}
