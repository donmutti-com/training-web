package com.donmutti.training.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Base class for all unit tests
 */
public abstract class BaseTest {

    // Valid URLs
    protected static final String URL_HTTPBIN      = "http://httpbin.org";
    protected static final String URL_SSL_HTTPBIN  = "https://httpbin.org";
    protected static final String URL_HTTPBIN_GET  = "http://httpbin.org/get";
    protected static final String URL_HTTPBIN_POST = "http://httpbin.org/post";

    // Invalid URLs
    protected static final String URL_FACEBOOK_NO_PROTOCOL      = "facebook.com";
    protected static final String URL_FACEBOOK_UNKNOWN_PROTOCOL = "unknown://facebook.com";

    // Methods
    protected static final String METHOD_GET    = "GET";
    protected static final String METHOD_POST   = "POST";
    protected static final String METHOD_PUT    = "PUT";
    protected static final String METHOD_DELETE = "DELETE";

    // Headers
    protected static final String HEADER_USER_AGENT      = "User-Agent";
    protected static final String HEADER_ACCEPT_LANGUAGE = "Accept-Language";

    // Header values
    protected static final String USER_AGENT      = "Mozilla/5.0";
    protected static final String ACCEPT_LANGUAGE = "en-US,en;q=0.5";

    // Logging
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    /**
     * Reads content of a stream into a string
     * @param inputStream input stream
     * @return string
     * @throws IOException
     */
    protected String readFromStream(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }
}
