package com.donmutti.training.web.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.security.cert.Certificate;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Testing HTTP operations with <a href="https://docs.oracle.com/javase/8/docs/api/java/net/HttpURLConnection.html">HttpURLConnection</a>.
 */
@RunWith(JUnit4.class)
public class HttpURLConnectionTest extends BaseHttpURLConnectionTest {

    @Test
    public void testCreateConnection() throws IOException {
        createConnection(URL_HTTPBIN);
    }

    @Test(expected = MalformedURLException.class)
    public void testCreateConnection_NoProtocol() throws Exception {
        createConnection(URL_FACEBOOK_NO_PROTOCOL);
    }

    @Test(expected = MalformedURLException.class)
    public void testCreateConnection_UnknownProtocol() throws IOException {
        createConnection(URL_FACEBOOK_UNKNOWN_PROTOCOL);
    }

    @Test
    public void testSslCreateConnection() throws IOException {
        createSslConnection(URL_SSL_HTTPBIN);
    }

    @Test(expected = ClassCastException.class)
    public void testSslCreateConnection_BadProtocol() throws IOException {
        createSslConnection(URL_HTTPBIN);
    }

    @Test
    public void testConnect() throws IOException {
        HttpURLConnection conn = createConnection(URL_HTTPBIN);
        connect(conn);
    }

    @Test
    public void testDisconnect() throws IOException {
        HttpURLConnection conn = createConnection(URL_HTTPBIN);
        connect(conn);
        disconnect(conn);
    }

    @Test
    public void testSslGetCertificates() throws IOException {
        HttpsURLConnection conn = createSslConnection(URL_SSL_HTTPBIN);
        connect(conn);

        // SSL info
        LOGGER.info("Cipher Suite: {}", conn.getCipherSuite());
        LOGGER.info("Local Principal: {}", conn.getLocalPrincipal());
        LOGGER.info("Peer Principal: {}", conn.getPeerPrincipal());

        // Certificates
        Certificate certs[] = conn.getServerCertificates();
        assertNotNull(certs);
        LOGGER.info("SSL Certificates:");
        IntStream.range(0, certs.length - 1).forEach(
            idx -> {
                Certificate cert = certs[idx];
                LOGGER.info("Cert #{} Type: {}", idx, cert.getType());
                LOGGER.info("Cert #{} Hash Code: {}", idx, cert.hashCode());
                LOGGER.info("Cert #{} Public Key Algorithm: {}", idx, cert.getPublicKey().getAlgorithm());
                LOGGER.info("Cert #{} Public Key Format: {}", idx, cert.getPublicKey().getFormat());
            });
    }

    @Test
    public void testGet() throws Exception {
        HttpsURLConnection conn = createSslConnection(URL_SSL_HTTPBIN);

        // Prep to GET request
        conn.setRequestMethod(METHOD_GET);
        conn.setRequestProperty(HEADER_USER_AGENT, USER_AGENT);

        // Send GET request
        int responseCode = getResponse(conn);
        assertEquals(200, responseCode);

        // Read the response
        String responseBody = readFromStream(conn.getInputStream());
        assertNotNull(responseBody);
        LOGGER.info(responseBody);
    }

    @Test
    public void testPost() throws Exception {
        HttpURLConnection conn = createConnection(URL_HTTPBIN_POST);

        // Prep to POST request
        conn.setRequestMethod(METHOD_POST);
        conn.setRequestProperty(HEADER_USER_AGENT, USER_AGENT);
        conn.setRequestProperty(HEADER_ACCEPT_LANGUAGE, ACCEPT_LANGUAGE);
        conn.setDoOutput(true);

        // Send POST request
        connect(conn);
        String queryString = "message=This+is+a+test+message";
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
        writer.write(queryString);
        writer.flush();
        writer.close();
        int responseCode = getResponse(conn);
        assertEquals(200, responseCode);

        // Read the response
        String responseBody = readFromStream(conn.getInputStream());
        assertNotNull(responseBody);
    }
}
